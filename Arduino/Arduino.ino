////////////Librerias///////////////////////////
#include <Metro.h>
////////////////////////////////////////////////
//hola mundo
////////////Arrays de pines/////////////////////
int transArray[3] = {2, 3, 4};
int sizeTransArray = sizeof(transArray) / sizeof(transArray[0]);
int watchArray[3] = {A0, A1, A2};
int sizewatchArray = sizeof(watchArray) / sizeof(watchArray[0]);
float watchArrayStatus[3];
////////////////////////////////////////////////
float sumatoria = 0;
/////////////Arrays de variables///////////////////////////////////
int metroDelay = 10000;
Metro transMetro = Metro(metroDelay);
///////////////////////////////////////////////////////////////////
int wachInFocus = 0;
static float Sensibilidad = 0.1;
void setup() {
  //Inicia serial para debuguear.
  Serial.begin(115200);

  //Pone como salida TODOS los pines de los transistores
  for (int i = 0; i < sizeTransArray; i++) {
    pinMode (transArray[i], OUTPUT);
  }
  //Pone como entrada TODOS los pines de los wacheadores
  for (int i = 0; i < sizewatchArray; i++) {
    pinMode (watchArray[i], INPUT);
  }

  //Realiza un breve parpadeo de los wacheadores
  //para indicar que estan siendo cotrolados por los transistores
  checkWatcheador();
}

void loop() {
  switchWatcheador();
  sensarWacheador();
  delay(100);
}

//Realiza un breve parpadeo de los wacheadores
//para indicar que estan siendo cotrolados por los transistores
void checkWatcheador() {
  boolean state = false;
  for (int o = 0; o < 6; o++) {
    state = !state;
    for (int i = 0; i < sizeTransArray; i++) {
      digitalWrite (transArray[i], state);
    }
    delay(300);
  }
}

//Intercambia el tiempo de lectura entre wacheadores
void switchWatcheador() {
  if (transMetro.check() == 1)
  {
//    Serial.print(wachInFocus * 2);
//    Serial.print(",");
//    if (watchArrayStatus[wachInFocus] > metroDelay / 22) {
//      Serial.println("Encendido");
//    } else {
//      Serial.println("Apagado");
//    }
    sumatoria = 0;
    digitalWrite(transArray[wachInFocus], false);
    transMetro.reset();
    wachInFocus ++;
    if (wachInFocus < sizewatchArray) {
    } else {
      wachInFocus = 0;
    }
    digitalWrite(transArray[wachInFocus], true);
  }
}

//Toma los datos del wacheador en foco
void sensarWacheador()
{
  float resultado = 0;
  int muestras = 50;
  int minimo = 20000;
  int maximo = 0;
  for (int i = 0; i <= muestras; i++) {
    float sensorValue = analogRead(watchArray[wachInFocus]);
    if (sensorValue > maximo)
    {
      maximo = sensorValue;

    }
    else if (sensorValue < minimo)
    {
      minimo = sensorValue;
    }
    /*   float resultado = sensorValue - 512;
       if (resultado < 0) {
         float resultado = resultado * -1;
    */
    //  }

  }
  resultado = maximo - minimo;

  if (resultado < 12) {
    sumatoria = sumatoria + resultado;
  } else {
    sumatoria = sumatoria + 5;
  }
  watchArrayStatus[wachInFocus]=sumatoria;
  Serial.print(wachInFocus*300);
  Serial.print(","); 
  Serial.println(watchArrayStatus[wachInFocus]);
  watchArrayStatus[wachInFocus] = sumatoria;
}
